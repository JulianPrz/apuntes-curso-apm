import requests
from bs4 import BeautifulSoup
import csv
import time
import random


root = 'https://elpais.com'
url = '/noticias/amianto/'
target_url=root+url
#userAgent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0'
sleepTimes = [0.2, 0.3, 0.4]
items = 1
page_num = 0

with open('amianto.csv', 'w', newline='', encoding='utf-8') as csv_file:
    writer = csv.writer(csv_file, delimiter=';')
    writer.writerow(['id_new','lang', 'author', 'description', 'keywords', 'news_keywords', 'dateScheme',
                    'date', 'published_time', 'modified_time', 'section', 'image', 'title', 'link', 'body'])

    while True:
        r = requests.get(target_url+str(page_num))
        if r.status_code == 404: # break once the page is not found
            break
        soup = BeautifulSoup(r.text,"lxml")
        rows = soup.select("h2")
        for row in rows:
            items += 1
        page_num += 1
        time.sleep(random.choice(sleepTimes))
    print("He encontrado "+str(items)+" noticias relacionadas con amianto en "+str(page_num)+" páginas")

    for x in range(0, page_num):
        r = requests.get(target_url+str(x))
        soup = BeautifulSoup(r.text, "lxml")
        rows = soup.select("h2")
        for row in rows:
            title = row.text.strip()
            link = row.find('a').get('href')
            r2 = requests.get(root+link)
            soup2 = BeautifulSoup(r2.text, "lxml")

            lang = soup2.find('meta', {"name": "lang"}).get('content')
            author = soup2.find('meta', {"name": "author"}).get('content')
            description = soup2.find(
                'meta', {"name": "description"}).get('content')
            keywords = soup2.find('meta', {"name": "keywords"}).get(
                'content') if (soup2.find('meta', {"name": "keywords"})) else ""
            news_keywords = soup2.find('meta', {"name": "news_keywords"}).get(
                'content') if (soup2.find('meta', {"name": "news_keywords"})) else ""
            dateScheme = soup2.find('meta', {"name": "date"}).get(
                'scheme') if (soup2.find('meta', {"name": "date"})) else ""
            date = soup2.find('meta', {"name": "date"}).get('content') if (
                soup2.find('meta', {"name": "date"})) else ""
            published_time = soup2.find(
                'meta', property="article:published_time").get('content')
            modified_time = soup2.find(
                'meta', property="article:modified_time").get('content')
            section = soup2.find(
                'meta', {"property": "article:section"}).get('content')
            image = soup2.find('meta', {"property": "og:image"}).get('content')

            if soup2.find(class_='a_b article_body | color_gray_dark'):
                bodyArticle = soup2.find(
                    class_='a_b article_body | color_gray_dark').find_all_next('p')[:-1]
            else:
                bodyArticle = soup2.find(
                    class_='articulo-cuerpo').find_all_next('p')[:-1]

            body = ""
            for b in bodyArticle:
                if b.text.strip()[:14] == "Este navegador":
                    print("salta!")
                    continue
                if b.text.strip()[:38] == "Puedes seguir a CLIMA Y MEDIO AMBIENTE":
                    print("salta2!")
                    continue

                body += b.text.strip()
            id_new=items
            print("Noticia "+str(id_new)+" escrapeada!")
            items-=1
            writer.writerow([id_new, lang, author, description, keywords, news_keywords, dateScheme,
                            date, published_time, modified_time, section, image, title, root+link, body])
            time.sleep(random.choice(sleepTimes))
        print("Página "+str(x)+" escrapeada!")
print("csv exportado! saliendo...")
