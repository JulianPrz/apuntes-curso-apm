import requests
from bs4 import BeautifulSoup
import csv
import time
import random

# Iniciamos variables
root = 'https://elpais.com'  # Url principal del medio
# Dirección de la página que muestra noticias relacionadas con la palabra amianto
url = '/noticias/amianto/'
target_url = root+url
# Lista de tiempos que utilizaremos más adelante para las peticiones
sleepTimes = [0.2, 0.3, 0.4]
items = 1  # Iniciamos variable items para contador de artículos
page_num = 0  # Variable para el contador de páginas

# Iniciamos archivo csv
# (nombre de archivo, w de writable, , codificación UTF-8)
with open('amianto.csv', 'w', newline='', encoding='utf-8') as csv_file:
    # Le decimos que el delimitador para tabular será ";"
    writer = csv.writer(csv_file, delimiter=';')
    # Escribimos primera línea, cabecera del archivo CSV
    writer.writerow(['id_new', 'lang', 'author', 'description', 'keywords', 'news_keywords', 'dateScheme',
                    'date', 'published_time', 'modified_time', 'section', 'image', 'title', 'link', 'body'])
    # Este bucle while True es infinito hasta que se interrumpa con un break
    # Lo utilizamos para que comprobar cuántos artículos hay en total y cuántas páginas hay en el total de la paginación. Para cuando el mensaje que recibe es de error al llegar a una página que no existe
    while True:
        r = requests.get(target_url+str(page_num))
        if r.status_code == 404:  # se interrumpe cuando la página no existe. Error 404
            break
        soup = BeautifulSoup(r.text, "lxml")
        rows = soup.select("h2")
        for row in rows:
            items += 1  # Va contando todos los items de una página
        page_num += 1  # Cuando termina de contarlos pasa a la siguiente página
        # Tiempo aleatorio para que el servidor no nos detecte como un proceso automatizado
        time.sleep(random.choice(sleepTimes))
    print("He encontrado "+str(items) +
          " noticias relacionadas con amianto en "+str(page_num)+" páginas")

    # El bucle for repite todo lo que hay dentro por el rango que le indiquemos, en este caso hasta el número de páginas que haya contado anteriormente. Si queremos hacer una primera prueba indicamos range(0,1)
    for x in range(0, page_num):
        # El patrón de paginación de la URL está basado en un número entero, por lo que tenemos que indicarselo en cada loop con el valor "x" y convertirlo a texto con str()
        r = requests.get(target_url+str(x))
        # Cargamos la información descargada de esa URL al objeto "soup"
        soup = BeautifulSoup(r.text, "lxml")
        # Seleccionamos todos los elementos "h2" de esa página, crea una lista con todos los elementos que la carga en la variable "rows"
        rows = soup.select("h2")
        # Este bucle for lo que hace es cargar cada elemento obtenido "h2" en la variable "row" y ejecutar lo que hay dentro del bucle, de esta manera itera por toda la lista
        for row in rows:
            # Con .text obtenemos el contenido de ese elemento, sino nos aparecería toda la información de la etiqueta h2, y con strip elimina cualquier espacio por delante o por atrás de la frase
            title = row.text.strip()
            # En cada elemento raw que están cargados los h2, tmb hay información del link a la noticia del propio titular, lo extraemos con .get('href') y lo cargamos en la variable "link"
            link = row.find('a').get('href')
            # Como tenemos ese link a la noticia, repetimos el proceso de descargarnos la info de esa página pero esta vez en la variable "r2"
            r2 = requests.get(root+link)
            # Pasamos esa información al objeto "soup2" para poder utilizar los métodos del módulo BeautifulSoup
            soup2 = BeautifulSoup(r2.text, "lxml")

            # Metadata. Información extraida de los elementos <meta> de la página
            lang = soup2.find('meta', {"name": "lang"}).get('content')
            author = soup2.find('meta', {"name": "author"}).get('content')
            description = soup2.find(
                'meta', {"name": "description"}).get('content')
            keywords = soup2.find('meta', {"name": "keywords"}).get(
                'content') if (soup2.find('meta', {"name": "keywords"})) else ""
            news_keywords = soup2.find('meta', {"name": "news_keywords"}).get(
                'content') if (soup2.find('meta', {"name": "news_keywords"})) else ""
            dateScheme = soup2.find('meta', {"name": "date"}).get(
                'scheme') if (soup2.find('meta', {"name": "date"})) else ""
            date = soup2.find('meta', {"name": "date"}).get('content') if (
                soup2.find('meta', {"name": "date"})) else ""
            published_time = soup2.find(
                'meta', property="article:published_time").get('content')
            modified_time = soup2.find(
                'meta', property="article:modified_time").get('content')
            section = soup2.find(
                'meta', {"property": "article:section"}).get('content')
            image = soup2.find('meta', {"property": "og:image"}).get('content')

            # CuerpoArticulo. Hay dos maneras de encontrar el cuerpo de la noticia en los artículos de elpaís, con la class="a_b article_body | color_gray_dark" o "articulo-cuerpo"
            if soup2.find(class_='a_b article_body | color_gray_dark'):
                # En todos ellos, el último elemento html es de publi. con [:-1] lo que le decimos es que escoja desde el primero ":" hasta el penúltimo "-1"
                bodyArticle = soup2.find(
                    class_='a_b article_body | color_gray_dark').find_all_next('p')[:-1]
            else:
                bodyArticle = soup2.find(
                    class_='articulo-cuerpo').find_all_next('p')[:-1]

            # Iniciamos la variable vacía "body" donde iremos concatenando los elementos extraidos
            body = ""
            # Cargamos cada elemento de la lista "bodyArticle" en la variable "b"
            for b in bodyArticle:
                # Esto es un ajuste porque muchos párrafos empezaban con estas frases que no son propias de la noticia. Si los encuentra, salta al siguiente elemento
                if b.text.strip()[:14] == "Este navegador":
                    print("salta!")
                    continue
                if b.text.strip()[:38] == "Puedes seguir a CLIMA Y MEDIO AMBIENTE":
                    print("salta2!")
                    continue
                # Operación para concatenar todos los elementos de párrafo en la misma variable "body"
                body += b.text.strip()
            id_new = items
            print("Noticia "+str(id_new)+" escrapeada!")
            items -= 1
            # Pasamos todos los valores extraidos en cada variable a la siguiente fila (row) en la tabla
            writer.writerow([id_new, lang, author, description, keywords, news_keywords, dateScheme,
                            date, published_time, modified_time, section, image, title, root+link, body])
            # Ponemos un tiempo random entre petición y petición para que el servidor no lo detecte como un proceso automatizado y parezca más humano
            # Escoge entre los que hemos indicado al inicio en la variable "sleepTimes"
            time.sleep(random.choice(sleepTimes))
        # Imprimimos un mensaje en consola para que nos vaya diciendo porqué página de los resultados va
        print("Página "+str(x)+" terminada!")
# Mensaje de finalización
print("csv exportado! saliendo...")
