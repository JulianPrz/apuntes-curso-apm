#Importamos a nuestro código el módulo requests que instalamos previamente
import requests
#Importamos el módulo bs4 que tmb instalamos previamente y lo vinculamos como BeautifulSoup para poder referenciarlo en nuestro código
from bs4 import BeautifulSoup

#Cargamos la url de donde vamos a extraer los datos a la variable "url"
url = "https://elpais.com/noticias/amianto/"
#Hacemos una peticion a la url y cargamos todo en crudo a la variable "r"
r = requests.get(url)
#Convertimos a texto todo lo extraido de "r" con la función r.text y se lo pasamos a BeautifulSoup(), de esta manera podremos realizar una serie de acciones con ese contenido
soup = BeautifulSoup(r.text, "lxml")
#Con .select() Seleccionamos todos los elementos HTML que tengan la etiqueta h2, me los devuelve como una lista de elementos y la cargamos en la variable "titles".
titles = soup.select("h2")

#Con el bucle for lo que hacemos es iterar por todos los elementos de una lista. En este caso "titles", que tiene cargados todos los elementos h2.
#Al iterar hace dos cosas: 1) Carga un elemento, empezando por el primero, en la variable que le digamos, en este caso "t"
#Y 2)En cada iteración, ejecuta el código que tenga dentro. De esta manera, carga el primer elemento h2 y lo que le decimos dentro del for, es que lo imprima en consola con print()
#Importante, para que python sepa qué codigo está dentro del bucle for, es importante que la siguientes líneas estén indentadas, esto es que, que comiencen con un espacio tabulado.
for t in titles:
        #Dentro de los elementos "h2" hay elementos "a" y podemos extraer la URL que está indicada en su atributo "href"
        #Al ser una única selección, lo indicamos con .select_one() y con .get() obtenemos el valor del atributo "href" que es la propia URL de la noticia
        link = t.select_one("a").get("href")
        #Con t.text obtenemos el contenido de texto del elemento "h2", si solo dejamos "t" a solas, aparecería toda la info del elemento. Haced la prueba
        #Como las URL son relativas, como por ej.: "/politica/el-metro-cierra-estacion.html" tenemos que añadirle "https://elpais.com" delante. Imprimimos titular + url
        print(t.text,"https://elpais.com"+link)
#Al empezar de nuevo en un espacio tabulado menos, indicamos que hemos salido de ese bucle y continuamos con el código
print("extracción completada!")        
